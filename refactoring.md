# Différentes stratégies de restructuration de code

## Martin Fowler

```mermaid
graph TD
      big[Grosse fonction ou méthode] --> split[Découpage en sous fonctions]
      split --> params[Paramétrez les fonctions]
      split --> toplevel[Des fonctions de haut niveau]
      split --> classes[Des classes pour séparer les responsabilités]
      split --> transform[Construire un flux de transformation]
```

## SOLID

* **Responsabilité unique (Single responsibility)** : une classe, une fonction ou une méthode doit avoir une et une seule responsabilité
* **Ouvert/fermé (Open/closed)** : une entité applicative (class, fonction, module ...) doit être ouverte à l'extension, mais fermée à la modification
* **Substitution de Liskov (Liskov substitution)** : une instance de type T doit pouvoir être remplacée par une instance de type G, tel que G sous-type de T, sans que cela ne modifie la cohérence du programme
* **Ségrégation des interfaces (Interface segregation)** : préférer plusieurs interfaces spécifiques pour chaque client plutôt qu'une seule interface générale
* **Inversion des dépendances (Dependency inversion principle)** : il faut dépendre des abstractions, pas des implémentations